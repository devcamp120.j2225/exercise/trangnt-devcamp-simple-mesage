//Import thu vien expressjs = import express from "express"
const { request } = require("express");
const express = require("express");

// khoi tao 1 app express
const app = express();

// khai bao cong chay project
const port = 8000;

// khai bao de app doc duoc body JSON
app.use(express.json());

// khai bai Api = callback function la 1 function tham so cua 1 function khac, nó sẽ được thực hiện khi function chủ được thực hiện
app.get("/", (request, response) => {
    let today = new Date();

    response.json({
        message: `Xin chao, hom nay la ngay ${today.getDate()} thang ${today.getMonth()+1} nam ${today.getFullYear()}`
    })
})

// khai bao Api dang get
app.get("/get-method", (req,res) => {
    res.json({
        message: "GET Method"
    })
})

// khai bao Api dang POST
app.get("/post-method", (req,res) => {
    res.json({
        message: "POST Method"
    })
})

// khai bao Api dang PUT
app.get("/put-method", (req,res) => {
    res.json({
        message: "PUT Method"
    })
})

// khai bao Api dang DELETE
app.get("/delete-method", (req,res) => {
    res.json({
        message: "DELETE Method"
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})

// request params - ap dung cho GET - PUT - DELETE
// request params la nhung tham so xuat hien trong tham so URL cuat API
app.get("/request-params/:param1/:param2", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json({
        param1,
        param2
    })
})

// request query - chi ap dung cho GET
// khi lay request query bat buoc phai validate 
app.get("/request-query", (req,res) => {
    let query = req.query;

    res.json({
        query
    })
})

// request body JSON - chi ap dung cho PUT - POST
// khi lay request body bat buoc phai validate 
// can khai bao them o dong 11
app.post("/request-body-json", (req,res) => {
    let body = req.body;
})